
# Full Text Search 

## Introduction 

Full-Text Search refers to the bunch of techniques to search stored documents and data from the database. It uses text indexes for searching the items which makes it a more advanced and efficient way of searching.

![ftsimg](imgFTS/mainimg.png)

## Search Engines 
    
    
Search engines examine the entire data and return the matching items.
Today there is a variety of search engines present in the market *such* as **Elasticsearch, Solr, Sphinx, Postgresql, MySQL**, etc.

## Comparision 

Let us compare Lucene, Solr & Elasticsearch :

> ###  1) Lucene

Apache Lucene is a **free and open-source search engine library** developed to empower mobile applications & websites with searching capabilities. It is written entirely in Java and is suitable for all applications which require full-text searching features. Both *Elasticsearch* and *Solr* are based on Lucene.

**Lucene Model Work Flow** -

![lucene work flow](imgFTS/lucene-new.png)


**Features** -

* **Scalable, High-Performance Indexing** - It allows the user to process large documents and index them in less time.
  * **Incremental indexing** - This allows users to add, change, or remove documents without re-indexing all contents within the index
  * **Require small RAM** - only 1MB heap
  * Over 150GB/hour on modern hardware
  
  

* **Powerful, Accurate and Efficient Search Algorithms**

  * Includes many **powerful query types**
  * **Fielded Searching** 
  * **Ranked Searching** - This allows the users to rank their search results according to their relevance
  * Sorting by any field
  * **Search And Merge Data From Multiple Indices** - Pieces of information are searched on multiple indices simultaneously and combined. 
  * **Flexible faceting, highlighting, joins, and result grouping**
  * Allows **simultaneous update and searching**


> ###  2) Solr

Solr is a high performance, scalable, ready to deploy, open-source search platform which is used to build search applications and widely used for enterprise search and analytics. It is built on Lucene and written in Java.

**Solr Model Work Flow** -

![solr work flow](imgFTS/solr.png)

**Apache Solr performs the following operations for searching a document -** 

* **Indexing  -** It converts the documents into a machine-readable format which is called Indexing.
* **Querying -** Understanding the terms of a query asked by the user. These terms can be images or keywords.
* **Mapping  -** It maps the user query to the documents stored in the database to find the related result.
* **Ranking  -** As soon as the engine searches the indexed documents, it ranks the outputs as per their relevance.

**Features** -
* **Restful APIs** - It is not mandatory to have Java programming skills to communicate with Solr. We can use restful services also.
* **Real-Time indexing**
* **Dynamic Clustering**
* Allows you to perform a **multilingual Keyword search**
* Database integration
* Building machine-learning models are easy
* **NoSQL features** - It can be used as a big data scale NOSQL database where we can distribute the search tasks along the cluster.
* **Rich Document Handling**(e.g., Word, PDF)
* Automatic Load Balancing
* Allows **Autocompletion** and **Geo-Spatial Search**
* Standards-Based Open Interfaces – XML, JSON, and HTTP
* Support Recommendations & Spell Suggestions 
* Built-in Security for Authentication and Authorization
* Specially optimized for high volume web Traffic
* Comprehensive HTML Admiration Interfaces
* Supports both Schema and Schemaless configuration
* **Faceted Search and Filtering** - It refers to the classification of the search results into various categories. Types -
   * Query faceting
   * Date faceting


> ###  3) Elasticsearch

Elasticsearch is a distributed, open-source search and analytics engine with an HTTP web interface. Elasticsearch allows to store, search, and analyze huge volumes of data and give back answers in milliseconds. It can be used for all types of data, including textual, numerical, geospatial, structured, and unstructured. It is built on Apache Lucene and written in Java.


![elasticsearch work flow](imgFTS/elastic.png)

**Features** -

* **Distributed and Highly Available Search Engine**

  * Each index is fully sharded with a configurable number of shards.
  * Each shard can have one or more replicas.
  * Search operations performed on any of the replica shards.

* **CLI Tool** - It provides several tools for configuring security and performing tasks from the command line.
* **Data Enrichment** - With a variety of analyzers, tokenizers, filters, and enrichment options, Elasticsearch turns raw data into valuable information.

* **Multi-tenant**

  * Support for more than one index.
  * Index level configuration (number of shards, index storage, etc.).
  * Various set of APIs
    * **Document APIs** - Used to perform CRUD operations (create, read, update, delete) on individual documents, or multiple documents.
    * **Aggregation APIs** - The aggregations framework helps in providing aggregated data based on the search query.
    * **Management APIs** - These are used to manage the Elasticsearch cluster programmatically.
  
* **Flexibility** - Apart from Full-Text Searching, it is also used for document storage, time series analysis and metrics, and geospatial analytics.
    
* **Management** - It allows the user to manage their clusters, nodes, indices and shards, and also the data held within.

* **Document Oriented**

  * No need for upfront schema definition.
  * Schema can be defined for customization of the indexing process.

* **Reliable and Asynchronous** 
* **Real-Time Search**
* **Built on Apache Lucene**
  * Each shard is a fully functional Lucene index
  * All the power of Lucene is easily exposed through simple configuration and plugins.


**Comparision Table :**

![compare chart](imgFTS/chart.png)

**Comparision Graph :**

![compare](imgFTS/comparision.png)


Reference :

* [wikipedia.org](https://en.wikipedia.org/wiki/Full-text_search)

* [lucene.apache.org](https://lucene.apache.org/core/)

* [elastic.co](https://www.elastic.co/)

* [tutorialspoint.com](https://www.tutorialspoint.com/apache_solr/apache_solr_overview.htm)

* [alachisoft.com](https://www.alachisoft.com/resources/docs/ncache/prog-guide/lucene-components.html)

* [guru99.com](https://www.guru99.com/apache-solr-tutorial.html)